using UnityEngine;

[System.Serializable]
public class GameData
{
    public int playerScore;
    public Vector3 playerPosition;
    public int grenadeAmmo;
    public int playerHealth;
}
