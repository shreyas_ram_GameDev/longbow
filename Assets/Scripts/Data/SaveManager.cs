using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using System;

namespace Nightmare
{

    public class SaveManager : MonoBehaviour
    {

        public static SaveManager instance { get; private set; }

        public GameData gameData = new GameData();
        private string saveFile;

        public Button loadButton;

        private void Awake()
        {
            if (instance != null && instance != this)
                Destroy(gameObject);
            else
                instance = this;

            DontDestroyOnLoad(gameObject);

            saveFile = Application.persistentDataPath + "/gamedata1.json";
            
        }

        private void Start() 
        {

            if (!File.Exists(saveFile))
            {
                loadButton.interactable = false;
            }
            else
            {
                loadButton.onClick.AddListener(LoadGame);
            }
            
            
        }

        public void readFile()
        {
            if (File.Exists(saveFile))
            {
                // Read the entire file and save its contents.
                string fileContents = File.ReadAllText(saveFile);

                // Deserialize the JSON data 
                //  into a pattern matching the GameData class.
                gameData = JsonUtility.FromJson<GameData>(fileContents);
            }
        }

        public void writeFile()
        {
            // Serialize the object into JSON and save string.
            string jsonString = JsonUtility.ToJson(gameData);

            // Write JSON to file.
            File.WriteAllText(saveFile, jsonString);
        }


        public void DeleteSaveFile()
        {
            if (File.Exists(saveFile))
            {
                Debug.Log("file deleted");
                File.Delete(saveFile);
            }
            
        }


        public void SaveGame()
        {
            // Debug.Log("Game Saved!");
            gameData.playerScore = ScoreManager.score;
            gameData.grenadeAmmo = GrenadeManager.grenades;
            gameData.playerHealth = PlayerHealth.Instance.currentHealth;
            gameData.playerPosition = PlayerMovement.Instance.gameObject.transform.position;

            writeFile();
        }

        private void LoadGame()
        {

            readFile();

            PlayerHealth.Instance.LoadHealthData(gameData.playerHealth);
            ScoreManager.Instance.LoadScoreData(gameData.playerScore);
            GrenadeManager.Instance.LoadGrenadeData(gameData.grenadeAmmo);
            PlayerMovement.Instance.LoadPlayerPosition(gameData.playerPosition );

            LevelManager.Instance.LoadNewGame();
        }

    }
}