﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Nightmare
{
    public class GrenadeManager : MonoBehaviour
    {
        public static int grenades;        


        Text gText;                      // Reference to the Text component.
        GameData gameData;
        public static GrenadeManager Instance;

        void Awake()
        {
            // Set up the reference.
            gText = GetComponent<Text>();
            // Reset the score.
            Instance = this;
        }

        void Update()
        {
            // Set the displayed text to be the word "Score" followed by the score value.
            gText.text = "Grenades: " + grenades;
        }

        public void LoadGrenadeData(int _grenadeAmount)
        {
            grenades = _grenadeAmount;
            gText.text = "Grenades: " + grenades;
        }
    }
}