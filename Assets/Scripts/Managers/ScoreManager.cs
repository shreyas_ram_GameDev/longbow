﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Nightmare
{
    public class ScoreManager : MonoBehaviour
    {
        
        public static int score;
        private int levelThreshhold;
        const int LEVEL_INCREASE = 300;
        Text sText;

        public static ScoreManager Instance;
        void Awake ()
        {
            sText = GetComponent <Text> ();

            levelThreshhold = LEVEL_INCREASE;

            Instance = this;
        }


        void Update ()
        {
            sText.text = "Score: " + score;
            if (score >= levelThreshhold)
            {
                AdvanceLevel();
            }
        }

        private void AdvanceLevel()
        {
            levelThreshhold = score + LEVEL_INCREASE;
            LevelManager lm = FindObjectOfType<LevelManager>();
            lm.AdvanceLevel();
        }


        public void LoadScoreData(int _score)
        {
            score = _score;
            sText.text = "Score: " + score;
        }
    }
}